 simple RSS reader web application
 =================================
 
 Requirements
 ------------

|                     | ver    |
|---------------------|--------|
| composer-plugin-api | 1.1.0  |
| ext-ctype           | ^7.1.3 |
| ext-iconv           | ^7.1.3 |
| ext-PDO             | ^7.1.3 |
| ext-tokenizer       | ^7.1.3 |
| ext-xml             | ^7.1.3 |
| php                 | ^7.1.3 |
| MySql               | any    |

 Install
 -------
 
 1. Clone repository ```git clone git@bitbucket.org:aleksejs1/rss.git```
 2. Go to project root ```cd rss```
 3. Configure ```.env``` file
 4. Install dependencies
    ```
    composer install
    ```
 5. Update database structure
    ```
    php bin/console doctrine:migrations:migrate
    ```

 Tests
 -----
 
 Run command:
 ```
 php bin/phpunit
 ```

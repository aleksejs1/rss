<?php

namespace App\Tests;

use App\Service\FrequentWords;
use PHPUnit\Framework\TestCase;

class FrequentWordsTest extends TestCase
{
    public function testNoExcludes()
    {
        $frequentWordsService = new FrequentWords();
        $result = $frequentWordsService->getMostFrequentWords('One two three');
        $this->assertCount(3, $result);
    }

    public function testFrequentResultCount()
    {
        $frequentWordsService = new FrequentWords();
        $result = $frequentWordsService->getMostFrequentWords('One two three', 2);
        $this->assertCount(2, $result);
    }

    public function testExcludes()
    {
        $frequentWordsService = new FrequentWords(['onE']);
        $result = $frequentWordsService->getMostFrequentWords('One one two three');
        $this->assertCount(2, $result);
        $this->assertArrayNotHasKey('One', $result);
        $this->assertArrayNotHasKey('one', $result);
    }

    public function testFrequentWordsCounts()
    {
        $frequentWordsService = new FrequentWords();
        $result = $frequentWordsService->getMostFrequentWords('One two two three three three');
        $this->assertCount(3, $result);
        $this->assertEquals(3, $result['three']);
    }

    public function testFrequentWordsSorting()
    {
        $frequentWordsService = new FrequentWords();
        $result = $frequentWordsService->getMostFrequentWords('One two two three three three');
        $this->assertCount(3, $result);
        $this->assertEquals('three', key(array_slice($result, 0, 1, true)));
        $this->assertEquals('two', key(array_slice($result, 1, 1, true)));
        $this->assertEquals('one', key(array_slice($result, 2, 1, true)));
    }

    public function testSubwords()
    {
        $frequentWordsService = new FrequentWords(['self']);
        $result = $frequentWordsService->getMostFrequentWords('selfish self');
        $this->assertCount(1, $result);
        $this->assertArrayHasKey('selfish', $result);
        $this->assertArrayNotHasKey('self', $result);
    }
}

<?php

namespace App\Tests;

use App\Service\RssDownloader;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RssDownloaderTest extends TestCase
{
    public function testGetContent()
    {
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)->disableOriginalConstructor()->getMock();
        $responseBody = 'body';
        $responseMock = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $responseMock->method('getContent')->willReturn($responseBody);
        $clientMock = $this->getMockBuilder(HttpClientInterface::class)->disableOriginalConstructor()->getMock();
        $clientMock->method('request')->willReturn($responseMock);

        $rssDownloader = new RssDownloader($clientMock, $loggerMock);
        $result = $rssDownloader->getContent('www.test.te');
        $this->assertEquals($responseBody, $result);
    }

    public function testException()
    {
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)->disableOriginalConstructor()->getMock();
        $clientMock = $this->getMockBuilder(HttpClientInterface::class)->disableOriginalConstructor()->getMock();
        $clientMock->method('request')->willThrowException(new TransportException());

        $rssDownloader = new RssDownloader($clientMock, $loggerMock);
        $result = $rssDownloader->getContent('www.test.te');
        $this->assertEquals('', $result);
        $this->assertTrue(true);
    }
}

<?php

namespace App\Tests;

use App\Service\TextExtractor;
use PHPUnit\Framework\TestCase;

class TextExtractorTest extends TestCase
{
    public function testSomething()
    {
        $textExtractor = new TextExtractor();
        $this->assertTrue(true);
    }

    public function testExtractText()
    {
        $textExtractor = new TextExtractor();
        $result = $textExtractor->extractText('<p>paragraph</p>', ['p']);
        $this->assertEquals('paragraph', $result);
        $result = $textExtractor->extractText('<p>paragraph</p>');
        $this->assertEquals('', $result);
        $result = $textExtractor->extractText('paragraph');
        $this->assertEquals('', $result);
        $result = $textExtractor->extractText('Text <p>paragraph</p>', ['p']);
        $this->assertEquals('paragraph', $result);
    }

    public function testExtractMultiTag()
    {
        $textExtractor = new TextExtractor();
        $result = $textExtractor->extractText('<h1>heading</h1><h2>author</h2><p>paragraph</p>', ['h1','p']);
        $this->assertStringContainsString('paragraph', $result);
        $this->assertStringContainsString('heading', $result);
        $this->assertStringNotContainsString('author', $result);

        $result = $textExtractor->extractText('text <h1>heading</h1><p>paragraph</p><p>another</p>', ['p']);
        $this->assertStringContainsString('paragraph', $result);
        $this->assertStringContainsString('another', $result);
        $this->assertStringNotContainsString('heading', $result);
        $this->assertStringNotContainsString('text', $result);
    }
}

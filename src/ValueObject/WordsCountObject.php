<?php

namespace App\ValueObject;

class WordsCountObject
{
    private $wordsCountArray;

    public function __construct(string $string)
    {
        $this->wordsCountArray = array_count_values(
            str_word_count($string, 1)
        );
    }

    /**
     * @param array $exclude
     * @return WordsCountObject
     */
    public function excludeWords(array $exclude): self
    {
        $this->wordsCountArray = array_diff_key($this->wordsCountArray, array_flip($exclude));

        return $this;
    }

    /**
     * @return WordsCountObject
     */
    public function sortAsc(): self
    {
        asort($this->wordsCountArray);

        return $this;
    }

    /**
     * @return WordsCountObject
     */
    public function sortDesc(): self
    {
        $this->sortAsc();
        $this->wordsCountArray = array_reverse($this->wordsCountArray);

        return $this;
    }

    /**
     * @param int|null $count
     * @return array
     */
    public function getWordCounts(?int $count = null): array
    {
        if ($count) {
            return array_slice($this->wordsCountArray, 0, $count);
        }

        return $this->wordsCountArray;
    }
}

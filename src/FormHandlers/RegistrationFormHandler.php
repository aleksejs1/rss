<?php

namespace App\FormHandlers;

use App\Entity\User;
use App\Service\UserManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class RegistrationFormHandler
{
    /** @var UserManager */
    private $userManager;

    /**
     * RegistrationFormHandler constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return bool
     */
    public function handle(FormInterface $form, Request $request): bool
    {
        $form->handleRequest($request);

        if (false === $form->isSubmitted() || false === $form->isValid()) {
            return false;
        }

        $this->processed($form);

        return true;
    }

    /**
     * @param FormInterface $form
     */
    private function processed(FormInterface $form): void
    {
        $user = $form->getData();
        $this->userManager->encodeAndSetPassword($user, $form->get('plainPassword')->getData());
        $this->userManager->saveUser($user);
    }
}

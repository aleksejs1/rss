<?php

namespace App\Service;

class TextExtractor
{
    /**
     * @param string $source
     * @param array $tags
     * @return string
     */
    public function extractText(string $source, array $tags = []): string
    {
        $extractedText = '';

        foreach ($tags as $tag) {
            $extractedText .= ' ' . $this->extractFromTags($tag, $source);
        }

        $htmlText = html_entity_decode($extractedText);
        $text = strip_tags ($htmlText);
        $cleanText = preg_replace('/[[:^print:]]/', '', $text);

        return trim($cleanText);
    }

    /**
     * @param string $tag
     * @param string $source
     * @return string
     */
    private function extractFromTags(string $tag, string $source)
    {
        preg_match_all('#<\s*?' . $tag . '\b[^>]*>(.*?)</' . $tag . '\b[^>]*>#s', $source, $extract);

        return implode(' ', $extract[1]);
    }
}

<?php

namespace App\Service;

use App\ValueObject\WordsCountObject;

class FrequentWords
{
    /** @var array */
    private $exclude;

    public function __construct(array $exclude = [])
    {
        $this->exclude = array_map('mb_strtolower', $exclude);
    }

    /**
     * @param string $string
     * @param int $count
     * @return array
     */
    public function getMostFrequentWords(string $string, int $count = 10): array
    {
        $wordsCount = new WordsCountObject(mb_strtolower($string));

        return $wordsCount
            ->excludeWords($this->exclude)
            ->sortDesc()
            ->getWordCounts($count)
        ;
    }
}

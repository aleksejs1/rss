<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\FormHandlers\RegistrationFormHandler;
use App\Security\AppAuthenticatorAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /**
     * @param Request $request
     * @param GuardAuthenticatorHandler $guardHandler
     * @param AppAuthenticatorAuthenticator $authenticator
     * @param RegistrationFormHandler $registrationFormHandler
     * @return Response
     */
    public function register(
        Request $request,
        GuardAuthenticatorHandler $guardHandler,
        AppAuthenticatorAuthenticator $authenticator,
        RegistrationFormHandler $registrationFormHandler
    ): Response {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_feed');
        }

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);

        if ($registrationFormHandler->handle($form, $request)) {
            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main'
            );
        }

        return $this->render('security/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}

<?php

namespace App\Controller;

use App\Service\FrequentWords;
use App\Service\RssDownloader;
use App\Service\TextExtractor;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

class AppController extends AbstractController
{
    /**
     * @param FrequentWords $frequentWordsService
     * @param TextExtractor $textExtractor
     * @param RssDownloader $rssDownloaderService
     * @param DecoderInterface $serializer
     * @param LoggerInterface $logger
     * @return Response
     */
    public function app(
        FrequentWords $frequentWordsService,
        TextExtractor $textExtractor,
        RssDownloader $rssDownloaderService,
        DecoderInterface $serializer,
        LoggerInterface $logger
    ): Response {
        $content = $rssDownloaderService->getContent($this->getParameter('rss_feed_url'));
        $text = $textExtractor->extractText($content, $this->getParameter('extract_text_tags'));
        try {
            $decodedData = $serializer->decode($content, 'xml');
        } catch (UnexpectedValueException $e) {
            $logger->warning('AppController xml decode error', ['message' => $e->getMessage()]);
            $decodedData = null;
        }

        $frequentWords = $frequentWordsService->getMostFrequentWords(
            $text,
            $this->getParameter('frequent_words_count')
        );

        return $this->render('app/rss_view.html.twig', [
            'data' => $decodedData,
            'words' => $frequentWords,
        ]);
    }
}

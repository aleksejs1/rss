<?php

namespace App\Controller;

use App\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    /**
     * @param Request $request
     * @param UserManager $userManager
     * @return JsonResponse
     */
    public function checkRegisteredEmail(Request $request, UserManager $userManager): JsonResponse
    {
        if (!$request->request->has('email')) {
            return new JsonResponse(['error' => 'No email parameter']);
        }

        return new JsonResponse(['emailExist' => $userManager->isUserExist($request->request->get('email'))]);
    }
}
